package com.luyene.edible_netherwart.lists;

import net.minecraft.item.Food;

public class FoodList {
	public static final Food ROASTED_NETHER_WART = (new Food.Builder().nutrition(2).saturationMod(0.4F)).build();
	public static final Food NETHER_WART_STEW = (new Food.Builder().nutrition(6).saturationMod(2.7F)).build();

}
