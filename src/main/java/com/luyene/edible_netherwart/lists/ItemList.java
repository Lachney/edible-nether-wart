package com.luyene.edible_netherwart.lists;

import net.minecraft.item.Item;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder("edible_netherwart")
public class ItemList
{
	public static final Item ROASTED_NETHER_WART = null;
	public static final Item NETHER_WART_STEW = null;
	public static final Item WITHER_BONE = null;
	public static final Item WITHER_BONEMEAL = null;
}
