package com.luyene.edible_netherwart.events;

import org.apache.logging.log4j.Logger;

import com.luyene.edible_netherwart.EdibleNetherwart;
import com.luyene.edible_netherwart.items.netherwart_stew;
import com.luyene.edible_netherwart.items.wither_bonemeal;
import com.luyene.edible_netherwart.lists.BlockList;
import com.luyene.edible_netherwart.lists.FoodList;
import com.luyene.edible_netherwart.lists.ItemList;

import net.minecraft.block.Block;
import net.minecraft.block.RotatedPillarBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
public class RegistryEvents 
{
	public static final Logger LOGGER = EdibleNetherwart.LOGGER;
	public static final String MOD_ID = EdibleNetherwart.MOD_ID;
	
	@SubscribeEvent
	public static void registerItems(final RegistryEvent.Register<Item> event)
	{
		event.getRegistry().registerAll
		(
				new Item(new Item.Properties().tab(ItemGroup.TAB_FOOD).food(FoodList.ROASTED_NETHER_WART)).setRegistryName(location("roasted_nether_wart")),
				new netherwart_stew(new Item.Properties().tab(ItemGroup.TAB_FOOD).stacksTo(1).food(FoodList.NETHER_WART_STEW)).setRegistryName(location("nether_wart_stew")),
				new Item(new Item.Properties().tab(ItemGroup.TAB_MISC)).setRegistryName(location("wither_bone")),
				new wither_bonemeal(new Item.Properties().tab(ItemGroup.TAB_MISC)).setRegistryName(location("wither_bonemeal")),
				new BlockItem(BlockList.WITHER_BONE_BLOCK, new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)).setRegistryName(location("wither_bone_block"))
		);
		

	}
	@SubscribeEvent
	public static void registerBlocks(final RegistryEvent.Register<Block> event)
	{
		event.getRegistry().registerAll
		(
				new RotatedPillarBlock (Block.Properties.of(Material.STONE, MaterialColor.SAND).strength(2.0F)).setRegistryName(location("wither_bone_block"))
		);
			
	}
	public static ResourceLocation location(String name)
	{
		return new ResourceLocation(MOD_ID, name);
	}
	

}
