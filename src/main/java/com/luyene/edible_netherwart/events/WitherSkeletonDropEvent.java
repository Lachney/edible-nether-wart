package com.luyene.edible_netherwart.events;

import java.util.Collection;

import com.luyene.edible_netherwart.EdibleNetherwart;
import com.luyene.edible_netherwart.lists.ItemList;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.WitherSkeletonEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = EdibleNetherwart.MOD_ID, bus = Bus.FORGE)
public class WitherSkeletonDropEvent {

	
	
	@SubscribeEvent
	public static void witherSkeletonDropEvent(LivingDropsEvent event) {
		LivingEntity livingEntity = event.getEntityLiving();
		World world = livingEntity.getCommandSenderWorld();
	    double x = livingEntity.getX();
	    double y = livingEntity.getY();
	    double z = livingEntity.getZ();
	    int wbonedrop = world.getRandom().nextInt(3);
		
		if (livingEntity instanceof WitherSkeletonEntity)
		{

			event.getDrops().add(new ItemEntity(world, x, y,z, new ItemStack(ItemList.WITHER_BONE, wbonedrop)));
		}
		
		
	}


	    
	}
	 


