package com.luyene.edible_netherwart.items;

import com.luyene.edible_netherwart.EdibleNetherwart;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.NetherWartBlock;
import net.minecraft.item.BoneMealItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class wither_bonemeal extends Item {

	public wither_bonemeal(Properties properties) {
		super(properties);
		// TODO Auto-generated constructor stub
	}
	
	public ActionResultType useOn(ItemUseContext context) {
	      World world = context.getLevel();
	      BlockPos blockpos = context.getClickedPos();
	      BlockPos blockpos1 = blockpos.relative(context.getClickedFace());
	      BlockState blockstate = world.getBlockState(blockpos);
	      
	      if (blockstate.getBlock() == Blocks.NETHER_WART && blockstate.getValue(NetherWartBlock.AGE) != 3) {
	    	  	int age = blockstate.getValue(NetherWartBlock.AGE);
				world.setBlockAndUpdate(blockpos, blockstate.setValue(NetherWartBlock.AGE, age + 1));
				 if (!world.isClientSide) {
			            world.levelEvent(2005, blockpos, 0);
			         }
				 context.getItemInHand().shrink(1);
			return ActionResultType.SUCCESS;
				
	    	  
	      }
	      if (BoneMealItem.applyBonemeal(context.getItemInHand(), world, blockpos, context.getPlayer())) {
	         if (!world.isClientSide) {
	            world.levelEvent(2005, blockpos, 0);
	         }

	         return ActionResultType.SUCCESS;
	      } else {
	         boolean flag = blockstate.isFaceSturdy(world, blockpos, context.getClickedFace());
	         if (flag && BoneMealItem.growWaterPlant(context.getItemInHand(), world, blockpos1, context.getClickedFace())) {
	            if (!world.isClientSide) {
	               world.levelEvent(2005, blockpos1, 0);
	            }

	            return ActionResultType.SUCCESS;
	         } else {
	            return ActionResultType.PASS;
	         }
	      }
	      
	   }
}
