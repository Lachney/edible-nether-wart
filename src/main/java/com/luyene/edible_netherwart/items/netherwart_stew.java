package com.luyene.edible_netherwart.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class netherwart_stew  extends Item {
	
	public netherwart_stew(Properties properties) {
		super(properties);
		// TODO Auto-generated constructor stub
	}

	public ItemStack finishUsingItem(ItemStack stack, World worldIn, LivingEntity entityLiving) {
	      ItemStack itemstack = super.finishUsingItem(stack, worldIn, entityLiving);
	      return entityLiving instanceof PlayerEntity && ((PlayerEntity)entityLiving).abilities.instabuild ? itemstack : new ItemStack(Items.BOWL);
	   }

}
